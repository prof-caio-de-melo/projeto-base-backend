package users

import "gitlab.com/prof-caio-de-melo/user-service/internal/profiles"

type Metadata map[string]string

type User struct {
  ID       string           `json:"id"      bson:"id"`
  Name     string           `json:"name"    bson:"name"`
  Email    string           `json:"email"   bson:"email"`
  Password string           `json:"pass"    bson:"password"`
  Metadata Metadata         `json:"meta"    bson:"metadata"`
  Profile  profiles.Profile `json:"profile" bson:"profile"` 
}

func newUser(id, name, email, password string, profile profiles.Profile, meta Metadata) *User {
  return &User{
    ID: id,
    Name: name,
    Email: email,
    Password: password,
    Profile: profile, 
    Metadata: meta,
  }
}

func (u *User) Authenticate(password string) bool {
  return u.Password == password
}

func (u *User) SetProfile(p profiles.Profile) {
  u.Profile = p
}

func (u *User) SetMetadataElement(key, value string) {
  u.Metadata[key] = value
}

func (u *User) GetMetadataElement(key string) (string, bool) {
  value, exists :=  u.Metadata[key]
  return value, exists
}